import { useNavigate } from "react-router-dom";
import "../App.css";
import "./pokemon.css";

// component that handles the displaying of a card that will contain a pokemon
// to be displayed
const Pokemon = ({ pokemon, pokemonNotFound }) => {
  const navigate = useNavigate();

  const handleCardClick = () => {
    navigate(`${pokemon.name}`);
  };
  return (
    <button
      className="card"
      onClick={handleCardClick}
    >
      {pokemonNotFound ? (
        <p>Pokemon not Found</p>
      ) : (
        <>
          <img
            src={pokemon.sprites.front_default}
            alt="pokemon"
          />
          <h2 className="card-title">{pokemon.name}</h2>
          <div>
            {pokemon.types.map((type) => {
              return (
                <span
                  className={`span__type type--${type.type.name}`}
                  key={pokemon.id + type.type.name}
                >
                  {type.type.name}
                </span>
              );
            })}
          </div>
        </>
      )}
    </button>
  );
};

export default Pokemon;
