import React from 'react'
import '../App.css';

const Footer = ({pokemonCount}) => {
  return (
    <footer className="footer"><b>{pokemonCount}</b> &nbsp;Pokemons Loaded</footer>
  )
}

export default Footer