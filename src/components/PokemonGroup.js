import React from "react";
import Pokemon from "./Pokemon";
import "../App.css";

const PokemonGroup = ({ pokemons, pokemonNotFound }) => {
  //COMPONENTS
  /**
   * Pokemon - handles the rendering of each pokemon in a group
   */

  return (
    <ul id="pokedex">
      {pokemons.map((pokemon) => (
        <Pokemon
          key={pokemon.id}
          pokemon={pokemon}
          pokemonNotFound={pokemonNotFound}
        />
      ))}
    </ul>
  );
};

export default PokemonGroup;
