import React from "react";
const errorImg = require("../404.png");

const ErrorPage = () => {
  return (
    <div className="error-container">
      <img
        src={errorImg}
        alt="404 message"
      />
    </div>
  );
};

export default ErrorPage;
