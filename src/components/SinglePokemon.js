import "./styles/SinglePokemon.css";
import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";

// this component handles the displaying of pokemon details
// this will be displayed similar to modals

const SinglePokemon = () => {
  const [pokemonData, setPokemonData] = useState(null);

  const { name } = useParams();
  const { type } = useParams();

  const navigate = useNavigate();

  // gets the information from the api of the selected pokemon
  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${name}`);
      const data = await response.json();
      setPokemonData(data);

      console.log(data);
    };

    fetchData();
  }, [name]);

  // handles the closing of the modal as the x button was clicked
  // will remove the modal-displayed pokemon card
  // and will go back to the the page before the selection of the
  // given pokemon
  const handleCloseModal = () => {
    navigate("/" + type);
  };

  return (
    <div className={`page-single-pokemon modal`}>
      <div className="modal-content">
        {pokemonData ? (
          <div className="main-container">
            <h2>{pokemonData.name.toUpperCase()}</h2>
            <div className="details">
              <div className="left">
                <img
                  className="image-animated"
                  src={pokemonData.sprites.front_default}
                  alt={pokemonData.name}
                />
              </div>
              <div className="right">
                <p>
                  Type(s):{" "}
                  <b>{pokemonData.types.map((e) => e.type.name).join(", ")}</b>
                </p>
                <p>
                  Item(s):{" "}
                  <b>
                    {pokemonData.held_items.map((e) => e.item.name).join(", ")}
                  </b>
                </p>
                <p>
                  Weight: <b>{pokemonData.weight}</b>
                </p>
                <p>
                  Height: <b>{pokemonData.height}</b>
                </p>
                <br />
                <div className="details-footer">
                  <p>Base Stats:</p>
                  <ul>
                    {pokemonData.stats.map((e) => {
                      return (
                        <li>
                          {" "}
                          {e.stat.name.trim()}: <b>{e.base_stat}</b>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <p>Loading...</p>
        )}
        <div className="modal-footer">
          <button onClick={handleCloseModal}>X</button>
        </div>
      </div>
    </div>
  );
};

export default SinglePokemon;
