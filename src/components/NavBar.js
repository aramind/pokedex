import React, { useEffect, useState } from "react";
import { Link, NavLink } from "react-router-dom";
import "./styles/NavBar.css";

// component that will handle creating and displaying of the navigation elements

const NavBar = () => {
  const [failedToLoad, setFailedToLoad] = useState(false);
  const [types, setTypes] = useState([]);

  // decided to get the name of types from what is currently available in the api records
  // rather than hardcoding, so that if name of types on the api changes, the name of types
  // used in this app will also change accordingly
  useEffect(() => {
    const getPokemons = async () => {
      setFailedToLoad(false);

      try {
        const res = await fetch(`https://pokeapi.co/api/v2/type`);

        if (!res.ok) {
          throw new Error(`Pokemons not found, Status: ${res.status}`);
        }

        const data = await res.json();

        setTypes(data.results);
      } catch (error) {
        setFailedToLoad(true);
      }
    };

    getPokemons();
  }, []);

  // prepare the name of types in the array
  // adds "all" to be displayed at the start of the diff links
  let labels = [];
  if (types.length > 0) {
    labels = types.map((e) => e.name);
  }
  labels.unshift("all");

  return (
    <nav className="nav">
      <div className=" App__title nav__title">
        <h1>
          <Link
            style={{ color: "#cc05e6" }}
            to="/"
          >
            Pokedex
          </Link>
        </h1>
      </div>

      <li className="nav__links">
        {labels.map((e) => (
          <NavLink
            className="nav__link"
            to={"/" + e}
          >
            {e}
          </NavLink>
        ))}
      </li>
    </nav>
  );
};

export default NavBar;
