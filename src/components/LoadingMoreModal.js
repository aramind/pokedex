import React from "react";
import { css } from "@emotion/react";
import { ClipLoader } from "react-spinners";

const LoadingMoreModal = () => {
  const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
  `;

  return (
    <div className="modal">
      <div>
        <ClipLoader
          color={"#000000"}
          loading={true}
          css={override}
          size={50}
        />
      </div>
      <h2>Loading...</h2>
    </div>
  );
};

export default LoadingMoreModal;
