import React, { useEffect, useState } from "react";
import { Outlet, useParams } from "react-router-dom";
import LoadingMoreModal from "../components/LoadingMoreModal";
import PokemonGroup from "../components/PokemonGroup";

const Pokemons = () => {
  const { type } = useParams();

  const [failedToLoad, setFailedToLoad] = useState(false);
  const [pokemonNames, setPokemonNames] = useState([]);
  const [offset, setOffset] = useState(0);
  const [pokemonNotFound, setPokemonNotFound] = useState(false);
  const [pokemons, setPokemons] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingMore, setIsLoadingMore] = useState(false);

  // fetches the pokemons of the selected type from poke api,
  // gets their names, and store it on the pokemonNames array to be accessed
  // at every request to load more via the load more button
  useEffect(() => {
    const fetchPokemonNames = async () => {
      setFailedToLoad(false);
      setIsLoading(true);

      try {
        const response = await fetch(`https://pokeapi.co/api/v2/type/${type}`);

        if (!response.ok) {
          throw new Error(
            `Pokemons of ${type} not found, Status: ${response.status}`
          );
        }

        const data = await response.json();
        setPokemonNames(data.pokemon.map((pokemon) => pokemon.pokemon.name));
      } catch (error) {
        setFailedToLoad(true);
      }

      setIsLoading(false);
    };

    fetchPokemonNames();
  }, [type]);

  // handles the fetching and displaying of pokemons of the given type
  useEffect(() => {
    const fetchMorePokemons = async () => {
      if (pokemonNames.length === 0) {
        return;
      }

      setIsLoadingMore(true);
      const newPokemons = [];

      for (let i = offset; i < offset + 20; i++) {
        if (i >= pokemonNames.length) {
          break;
        }

        let pokemonName = pokemonNames[i];

        try {
          const response = await fetch(
            `https://pokeapi.co/api/v2/pokemon/${pokemonName}`
          );

          if (!response.ok) {
            throw new Error(
              `Pokemon ${pokemonName} not found, Status: ${response.status}`
            );
          }

          const pokemonData = await response.json();
          newPokemons.push(pokemonData);
        } catch (error) {
          setPokemonNotFound(true);
        }
      }

      setPokemons((prevPokemons) => [...prevPokemons, ...newPokemons]);
      setIsLoadingMore(false);
    };

    if (offset > 0 && offset % 20 === 0) {
      fetchMorePokemons();
    }
  }, [offset, pokemonNames]);

  // handler for the load more button
  // adds 20 to offset and will trigger a displaying of additional 20 pokemons
  const handleLoadMore = () => {
    setOffset((prevOffset) => prevOffset + 20);
  };

  // fetches and display the initial 20 pokemons of the given type
  // as the page renders when type was clicked on the navbar
  useEffect(() => {
    const fetchInitialPokemons = async () => {
      if (pokemonNames.length === 0) {
        return;
      }

      const newPokemons = [];
      for (let i = 0; i < 20 && i < pokemonNames.length; i++) {
        let pokemonName = pokemonNames[i];

        try {
          const response = await fetch(
            `https://pokeapi.co/api/v2/pokemon/${pokemonName}`
          );

          if (!response.ok) {
            throw new Error(
              `Pokemon ${pokemonName} not found, Status: ${response.status}`
            );
          }

          const pokemonData = await response.json();
          newPokemons.push(pokemonData);
        } catch (error) {
          setPokemonNotFound(true);
        }
      }

      setPokemons(newPokemons);
    };

    fetchInitialPokemons();
  }, [pokemonNames]);

  return (
    <>
      <Outlet />
      {failedToLoad ? (
        <p>Failed to load Pokemons</p>
      ) : (
        <div className="App">
          {isLoading ? (
            <LoadingMoreModal />
          ) : (
            <>
              <h1>Pokemons of type {type}</h1>
              <PokemonGroup pokemons={pokemons} />
              {pokemonNotFound && <p>Failed to load some Pokemons</p>}
              {isLoadingMore && <LoadingMoreModal />}
              {pokemons.length < pokemonNames.length && (
                <button
                  className="load-btn-type"
                  onClick={handleLoadMore}
                >
                  {isLoadingMore
                    ? `Loading more ${type.toUpperCase()} Pokemons...`
                    : `Load More ${type.toUpperCase()} Pokemons`}
                </button>
              )}
            </>
          )}
        </div>
      )}
    </>
  );
};
export default Pokemons;
