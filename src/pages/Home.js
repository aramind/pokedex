import React from "react";
import "./pages.css";
import home from "../imgs/home-image.jpg";

const Home = () => {
  console.log("rendering home...");
  return (
    <div className="home-page">
      {/* <p>by R. Miranda</p> */}
      <img
        src={home}
        alt="pokemon"
      />
    </div>
  );
};

export default Home;
