import React, { useEffect, useState } from "react";
import "./App.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import MainLayout from "./layouts/MainLayout";
import AppHome from "./AppHome";
import Pokemons from "./pages/Pokemons";
import Pokemon from "./components/Pokemon";
import SinglePokemon from "./components/SinglePokemon";
import Home from "./pages/Home";

// Router
const router = createBrowserRouter([
  {
    path: "/",
    element: <MainLayout />,
    children: [
      {
        path: "",
        element: <Home />,
      },
      {
        path: "all",
        // element: <AppHome />,
        element: <AppHome />,
      },
      {
        path: ":type",
        element: <Pokemons />,
        children: [
          {
            path: ":name",
            element: <SinglePokemon />,
          },
        ],
      },
    ],
  },
]);
function App() {
  return (
    <RouterProvider router={router}>
      <MainLayout />
    </RouterProvider>
  );
}

export default App;
