import React, { useEffect, useState } from "react";
import "./App.css";
import Footer from "./components/Footer";
import PokemonGroup from "./components/PokemonGroup";
import ErrorPage from "./components/ErrorPage";

function AppHome() {
  // UTILITY VARIABLES
  const CURRENT_MAX_POKEMON = 1279;
  const pokemonsPerLoad = 20;
  /***************************  STATES  ***************************************
   * pokemonCount - stores the number of pokemons fetched since the app starts
   *        (incremented by {pokemonsPerLoad} on every click of load more button)
   * limit - stores the number of pokemons to be fetched at each load
   *      - will change if it reaches the last group and that last group is less than
   *        {pokemonsPerLoad}
   * pokemons - stores the pokemon objects fetched since the app starts
   *        (incremented by {pokemonsPerLoad} on every click of load more button)
   * failedToLoad - stores the status of fetching the group of pokemons
   * pokemonNotFound - stores the status of fetching a pokemon
   **************************************************************************/
  const [pokemonCount, setPokemonCount] = useState(0);
  const [limit, setLimit] = useState(pokemonsPerLoad);
  const [pokemons, setPokemons] = useState([]);
  const [failedToLoad, setFailedToLoad] = useState(false);
  const [pokemonNotFound, setPokemonNotFound] = useState(false);

  // handles the loading of the initial pokemons
  let isMounted = false;
  useEffect(() => {
    const controller = new AbortController();
    const signal = controller.signal;

    if (isMounted) {
      getPokemons(signal);
    }
    return () => {
      // setFailedToLoad(true);
      isMounted = true;
      controller.abort();
    };
  }, []);

  // sets the scroll of the page so that the load button will be visible in every load of pokemons
  // useEffect(() => {
  //   window.scrollTo(0, document.body.scrollHeight);
  // }, [pokemons]);

  // displays the pokemons fetched
  const displayPokemon = (data) => {
    setPokemons((prevPokemons) => [...prevPokemons, data]);
  };

  // fetches each pokemon in the group called from API
  const getPokemon = async (pokemon, signal) => {
    setPokemonNotFound(false);
    try {
      const response = await fetch(
        `https://pokeapi.co/api/v2/pokemon/${pokemon}`
      );
      if (!response.ok) {
        throw new Error(`Pokemon was not found, Status: ${response.status}`);
      }
      const data = await response.json();
      displayPokemon(data);
    } catch (error) {
      setPokemonNotFound(true);
      displayPokemon(false);
    }
  };

  // fetches a group of pokemon from the API
  // limit - sets the number of pokemon in the group
  // pokemonCount - sets where the fetching will start
  const getPokemons = async (signal) => {
    setFailedToLoad(false);
    // console.log(`GET POKEMONS`);
    try {
      const response = await fetch(
        `https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${pokemonCount}`
      );

      if (!response.ok) {
        throw new Error(`Pokemons not found, Status: ${response.status}`);
      }

      const data = await response.json();
      // console.log("data", data);
      // calling getPokemon to fetch each pokemon in the group
      data.results.forEach((e) => {
        getPokemon(e.name, signal);
      });

      // checks if the next group to be fetched is the last group or not
      if (CURRENT_MAX_POKEMON - pokemonCount < pokemonsPerLoad) {
        setLimit(1279 - pokemonCount);
        setPokemonCount(CURRENT_MAX_POKEMON);
        console.log(
          `limit: ${
            CURRENT_MAX_POKEMON - pokemonCount
          } and pokemon count: ${CURRENT_MAX_POKEMON} `
        );
      } else {
        setPokemonCount(pokemonCount + pokemonsPerLoad);
        console.log(
          `limit: ${limit} and pokemon count: ${
            pokemonCount + pokemonsPerLoad
          } `
        );
      }
    } catch (error) {
      setFailedToLoad(true);
    }
  };

  //COMPONENTS
  /**
   * PokemonGroup - handles the rendering of the group of pokemons fetched
   * Footer - dynamically shows the total number of pokemons loaded
   */
  return (
    <>
      {failedToLoad ? (
        <>
          <ErrorPage />
        </>
      ) : (
        <>
          {/* <div className="App__title">
            <h1>Pokedex</h1>
          </div> */}
          <div className="App">
            <PokemonGroup
              pokemons={pokemons}
              pokemonNotFound={pokemonNotFound}
            />
            <button
              className={
                pokemonCount >= CURRENT_MAX_POKEMON
                  ? "btn btn-load disabled"
                  : "btn btn-load"
              }
              onClick={getPokemons}
              disabled={pokemonCount >= CURRENT_MAX_POKEMON}
            >
              Load {limit} More Pokemons
            </button>
          </div>
          <Footer pokemonCount={pokemonCount} />
        </>
      )}
    </>
  );
}

export default AppHome;
